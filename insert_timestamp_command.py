import sublime
import sublime_plugin
from datetime import datetime, timezone, timedelta


class InsertTimestampCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		offset = self.view.sel()[0]
		tz_est = timezone(timedelta(hours=-5), "EST") 
		datetime_fmt = "%A %m/%d/%Y %I:%M:%S%p %Z"
		now = datetime.now(tz=tz_est)
		self.view.insert(edit, offset.begin(), now.strftime(datetime_fmt))
